#!/bin/bash

# Find all plugin directories with a .git folder and check for updates
find ~/.vim/pack/myplugins/start/ -name ".git" -type d -execdir pwd \; -execdir bash -c "git pull" \;
