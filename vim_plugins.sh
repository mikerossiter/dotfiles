#!/bin/bash

# Change to the directory where the plugins are stored
cd ~/.vim/pack/myplugins/start/

# Initialize an empty array to store the plugin names
plugins=()

# Loop through each directory in the start directory
for d in */ ; do
  # Check if the directory is a Git repository
  if [ -d "${d}.git" ]; then
    # Extract the plugin name from the directory name
    plugin_name=$(basename "$d")
    # Add the plugin name to the array
    plugins+=("$plugin_name")
  fi
done

# Check if the plugin list file exists
if [ -f ~/.vim/plugin_list.txt ]; then
  # Read the saved list of plugins into an array
  readarray -t saved_plugins < ~/.vim/plugin_list.txt

  # Loop through each saved plugin
  for plugin in "${saved_plugins[@]}"; do
    # Check if the plugin is still installed
    if [[ ! " ${plugins[@]} " =~ " ${plugin} " ]]; then
      # Plugin has been removed
      echo "Plugin '${plugin}' has been removed."
    fi
  done

  # Loop through each current plugin
  for plugin in "${plugins[@]}"; do
    # Check if the plugin is new
    if [[ ! " ${saved_plugins[@]} " =~ " ${plugin} " ]]; then
      # Plugin is new
      echo "Plugin '${plugin}' is new."
    fi
  done
fi

# Save the list of plugins to a file and repo folder
printf '%s\n' "${plugins[@]}" > ~/.vim/plugin_list.txt
