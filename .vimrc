set nocompatible              " be iMproved, required
set autochdir " Sets save location to that of the current file
set ic " Ignore case when searching words
" set spell spelllang=en_gb
colorscheme slate
" Set tmp location for swp files
:set directory=/tmp//
"
call plug#begin()
" The default plugin directory will be as follows:
"   - Vim (Linux/macOS): '~/.vim/plugged'
"   - Vim (Windows): '~/vimfiles/plugged'
"   - Neovim (Linux/macOS/Windows): stdpath('data') . '/plugged'
" You can specify a custom plugin directory by passing it as the argument
"   - e.g. `call plug#begin('~/.vim/plugged')`
"   - Avoid using standard Vim directory names like 'plugin'
"
" Make sure you use single quotes
Plug 'junegunn/vim-easy-align'
Plug 'tpope/vim-fugitive'
Plug 'mileszs/ack.vim'
Plug 'w0rp/ale'
Plug 'wincent/command-t'
Plug 'shougo/deoplete.nvim'
Plug 'tfnico/vim-gradle'
Plug 'yggdroot/indentline'
Plug 'preservim/nerdtree'
Plug 'rust-lang/rust.vim'
Plug 'rstacruz/sparkup'
Plug 'ervandew/supertab'
Plug 'scrooloose/syntastic'
Plug 'fatih/vim-go'
Plug 'towolf/vim-helm'
Plug 'thanethomson/vim-jenkinsfile'
Plug 'andrewstuart/vim-kubernetes'
Plug 'hashivim/vim-terraform'
" Initialize plugin system
" - Automatically executes `filetype plugin indent on` and `syntax enable`.
call plug#end()
" You can revert the settings after the call like so:
"   filetype indent off   " Disable file-type-specific indentation
"   syntax off            " Disable syntax highlighting
"
" NERDTREE STUFF
" Start NERDTree. If a file is specified, move the cursor to its window.
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * NERDTree | if argc() > 0 || exists("s:std_in") | wincmd p | endif
"
" Let NERDTree see hidden files by default
let g:NERDTreeShowHidden=1
"
let vim_markdown_preview_github=1
set number
"
" botright term ++rows=8
" Enable type file detection. Vim will be able to try to detect the type of file in use.
filetype on
"
" Enable plugins and load plugin for the detected file type.
filetype plugin on

" Load an indent file for the detected file type.
filetype indent on
filetype plugin indent on
" Fix indentation issues
set paste
" Turn syntax highlighting on.
syntax on

" Highlight cursor line underneath the cursor horizontally.
set cursorline

" Highlight cursor line underneath the cursor vertically.
set cursorcolumn
" Set shift width to 4 spaces.
set shiftwidth=4

" Set tab width to 4 columns.
set tabstop=4

set redrawtime=10000

" Use space characters instead of tabs.
set expandtab

" Do not save backup files.
set nobackup

" Do not let cursor scroll below or above N number of lines when scrolling.
set scrolloff=10

" Do not wrap lines. Allow long lines to extend as far as the line goes.
set nowrap

" While searching though a file incrementally highlight matching characters as you type.
set incsearch

" Ignore capital letters during search.
set ignorecase

" Override the ignorecase option if searching for capital letters.
" This will allow you to search specifically for capital letters.
set smartcase

" Show partial command you type in the last line of the screen.
set showcmd

" Show the mode you are on the last line.
set showmode

" Show matching words during a search.
set showmatch

" Use highlighting when doing a search.
set hlsearch

" Set the commands to save in history default number is 20.
set history=1000
" Enable auto completion menu after pressing TAB.
set wildmenu

" Make wildmenu behave like similar to Bash completion.
set wildmode=list:longest

" There are certain files that we would never want to edit with Vim.
" Wildmenu will ignore files with these extensions.
set wildignore=*.docx,*.jpg,*.png,*.gif,*.pdf,*.pyc,*.exe,*.flv,*.img,*.xlsx

set statusline+=%F
set laststatus=2

" let g:SuperTabMappingForward = '<c-tab>'        
" let g:SuperTabMappingBackward = '<s-c-tab>' 

" Word wrap on. Turn off with set nowrap

set wrap

" I still like to use the mouse sorry!
set mouse=a

let g:go_def_mode='gopls'
let g:go_info_mode='gopls'
" Disable quote concealing in JSON files
let g:vim_json_conceal=0

packloadall
